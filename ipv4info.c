/*****************************************************************************
 * ipv4info.c
 * IPv4 address information
 ******************************************************************************/

#include "ipv4info.h"

#define IPV4_ADDR_SIZE  32
#define MASK_BLOCK_SIZE 8
#define BYTE_SIZE       8

#define ADDR "120.150.21.95/12"


int
main ()
{
  /* parsing string to ipaddr_s structure */
  char ip_str[] = ADDR;
  ipaddr_s addr = str_to_addr (ip_str);

  ipv4class_e netclass = get_class (&addr);
  private_e private_stat = is_public (&addr);

  /* Text output */
  print_addr_class (netclass);
  print_private (private_stat);
  /* More than text output */
  print_info_table (&addr);

  return EXIT_SUCCESS;
}

ipaddr_s
str_to_addr (char *str)
{
  ipaddr_s as;
  unsigned char prefix;

  as.fst = atoi (strtok (str, "."));
  as.sec = atoi (strtok (NULL, "."));
  as.trd = atoi (strtok (NULL, "."));
  as.fth = atoi (strtok (NULL, "/"));
  prefix = atoi (strtok (NULL, ""));
  as.mask = process_mask (prefix);

  return as;
}

mask_s
process_mask (unsigned char prefix)
{
  mask_s m;

  uint32_t bitmask = -1 << (IPV4_ADDR_SIZE - prefix);

  /* dividing bitmask into bytes in reverse order because bitmask is
   * reversed */
  uint8_t *byte_order[] = { &m.fth, &m.trd, &m.sec, &m.fst };
  int array_len = sizeof (byte_order) / sizeof (byte_order[0]);

  int i = 0;

  for (; i < array_len; i++, bitmask >>= MASK_BLOCK_SIZE)
    *byte_order[i] = bitmask;

  m.prefix = prefix;

  return m;
}

ipv4class_e
get_class (const ipaddr_s * const a)
{
  unsigned char f = a->fst;

  if ((f & 0b10000000) == 0b00000000)
    return class_a;
  if ((f & 0b11000000) == 0b10000000)
    return class_b;
  if ((f & 0b11100000) == 0b11000000)
    return class_c;
  if ((f & 0b11110000) == 0b11100000)
    return class_d;
  if ((f & 0b11111000) == 0b11110000)
    return class_e;

  return class_error;
}

/*  IPv4 have three private address blocks:
 *     10.  0.0.0/8
 *    172. 16.0.0/12
 *    192.168.0.0/16
 *  If address inside this block it's means that network is private.
*/

private_e
is_public (const ipaddr_s * const a)
{
  return
    /* for 10.0.0.0 - 10.255.255.255     */
    a->fst == 10 ||
    /* for 172.16.0.0 - 172.31.255.255   */
    (a->fst == 172 && (a->sec >= 16 && a->sec <= 31)) ||
    /* for 192.168.0.0 - 192.168.255.255 */
    (a->fst == 192 && a->sec == 168) ? private : public;
}

  /* Text output */

void
print_addr_class (ipv4class_e c)
{
  printf ("Network class: %c;\n", c);
}

void
print_private (private_e p)
{
  printf ("%s network;\n", p == public ? "Public" : "Private");
}

  /* More than text output */
void
print_info_table (const ipaddr_s * const a)
{
  printf ("Host Address                 (decimal): %3d.%3d.%3d.%3d\n",
	  a->fst, a->sec, a->trd, a->fth);
  printf ("Mask                         (decimal): %3d.%3d.%3d.%3d\n",
	  a->mask.fst, a->mask.sec, a->mask.trd, a->mask.fth);

  /* I'm not sure that it's right */
  printf ("First availible host address (decimal): %3d.%3d.%3d.%3d\n",
	  a->fst & a->mask.fst, a->sec & a->mask.sec,
	  a->trd & a->mask.trd, (a->fth & a->mask.fth) + 1);
  printf ("Last  availible host address (decimal): %3d.%3d.%3d.%3d\n",
	  (a->fst & a->mask.fst) | (uint8_t) (~a->mask.fst),
	  (a->sec & a->mask.sec) | (uint8_t) (~a->mask.sec),
	  (a->trd & a->mask.trd) | (uint8_t) (~a->mask.trd),
	  ((a->fth & a->mask.fth) | (uint8_t) (~a->mask.fth)) - 1);

  printf ("Broadcast            address (decimal): %3d.%3d.%3d.%3d\n",
	  (a->fst & a->mask.fst) | (uint8_t) (~a->mask.fst),
	  (a->sec & a->mask.sec) | (uint8_t) (~a->mask.sec),
	  (a->trd & a->mask.trd) | (uint8_t) (~a->mask.trd),
	  (a->fth & a->mask.fth) | (uint8_t) (~a->mask.fth));

  /* Strange binary output */
  printf ("Host Address                 (binary ): ");
  print_binary_uint8 (a->fst);
  printf (".");
  print_binary_uint8 (a->sec);
  printf (".");
  print_binary_uint8 (a->trd);
  printf (".");
  print_binary_uint8 (a->fth);
  printf ("\n");

  printf ("Mask                         (binary ): ");
  print_binary_uint8 (a->mask.fst);
  printf (".");
  print_binary_uint8 (a->mask.sec);
  printf (".");
  print_binary_uint8 (a->mask.trd);
  printf (".");
  print_binary_uint8 (a->mask.fth);
  printf ("\n");

  printf ("First availible host address (binary ): ");
  print_binary_uint8 (a->fst & a->mask.fst);
  printf (".");
  print_binary_uint8 (a->sec & a->mask.sec);
  printf (".");
  print_binary_uint8 (a->trd & a->mask.trd);
  printf (".");
  print_binary_uint8 ((a->fth & a->mask.fth) + 1);
  printf ("\n");

  printf ("Last  availible host address (binaty ): ");
  print_binary_uint8 ((a->fst & a->mask.fst) | (uint8_t) (~a->mask.fst));
  printf (".");
  print_binary_uint8 ((a->sec & a->mask.sec) | (uint8_t) (~a->mask.sec));
  printf (".");
  print_binary_uint8 ((a->trd & a->mask.trd) | (uint8_t) (~a->mask.trd));
  printf (".");
  print_binary_uint8 (((a->fth & a->mask.fth) | (uint8_t) (~a->mask.fth)) -
		      1);
  printf ("\n");

  printf ("Broadcast            address (binary ): ");
  print_binary_uint8 ((a->fst & a->mask.fst) | (uint8_t) (~a->mask.fst));
  printf (".");
  print_binary_uint8 ((a->sec & a->mask.sec) | (uint8_t) (~a->mask.sec));
  printf (".");
  print_binary_uint8 ((a->trd & a->mask.trd) | (uint8_t) (~a->mask.trd));
  printf (".");
  print_binary_uint8 ((a->fth & a->mask.fth) | (uint8_t) (~a->mask.fth));
  printf ("\n");

  printf ("Availible number of address for hosts : %d\n",
	  (1 << (IPV4_ADDR_SIZE - a->mask.prefix)) - 2);
}

void
print_binary_uint8 (uint8_t d)
{
  int i = 0;
  for (; i < BYTE_SIZE; i++, d <<= 1)
    printf ("%c", d & 0x80 ? '1' : '0');
}
