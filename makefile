NAME = ipv4info
CC = gcc
CFLAGS = -g -Wall
RELEASE_CFLAGS = -O3 -Wall

ipv4info: ipv4info.c ipv4info.h
	$(CC) $(CFLAGS) $< -o $@

release: ipv4info.c ipv4info.h
	$(CC) $(RELEASE_CFLAGS) $< -o $(NAME)

clean:
	rm *.o
