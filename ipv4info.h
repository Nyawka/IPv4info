#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>

typedef struct mask
{
  uint8_t fst;
  uint8_t sec;
  uint8_t trd;
  uint8_t fth;
  uint8_t prefix;
} mask_s;

typedef struct ipaddr
{
  uint8_t fst;
  uint8_t sec;
  uint8_t trd;
  uint8_t fth;
  mask_s mask;
} ipaddr_s;

typedef enum ipv4_class
{
  class_error = 0,
  class_a = 'A',
  class_b = 'B',
  class_c = 'C',
  class_d = 'D',
  class_e = 'E'
} ipv4class_e;

typedef enum private_addr
{
  private,
  public
} private_e;


ipaddr_s str_to_addr (char *str);
mask_s process_mask (uint8_t prefix);
ipv4class_e get_class (const ipaddr_s * const a);
private_e is_public (const ipaddr_s * const a);

  /* Text output */
void print_addr_class (ipv4class_e c);
void print_private (private_e p);
void print_info_table (const ipaddr_s * const a);
void print_binary_uint8 (uint8_t d);
